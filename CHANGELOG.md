## CHANGELOG

Better description of changes on each release.

### 0.1.0-beta6

- Allow to be installed on Python 3.8.

### 0.1.0-beta5

- Fixes typos and errors.

### 0.1.0-beta2

- Created a script to be run as a command.
- Created a new logger configurations for cmd.
- Usage of the auto sanitize via command to make it usage by external applications.

### 0.1.0-beta1

- First draft of the project structure and functionality.
- Erasures working for HDD and SSD, validation with some error handling.