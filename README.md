# Usody Sanitize

> Under development.

This tool securely erases disks by performing a certificate-based validation of
the wipe process. It ensures that the data on the disk is completely and 
irrecoverably erased, protecting sensitive information from being recovered. 
The tool uses industry-standard wiping methods to securely erase the data on 
the disk, making it impossible to recover. The tool also generates a 
certificate of sanitize process that can be used to verify the authenticity of the wipe
process. This tool is perfect for businesses and individuals who need to
securely and permanently remove sensitive data from their disks.

## Todo

- Test the command with more errors on erasures.
- Better command output handler.
- Improve the export of the erasures into json files.

## Installation

Install the package from the official PyPi repository:

<div class="termy">

```console
$ pip install usody_sanitize

---> 100%
```

</div>

## Usage

You can use this module via terminal or calling it from an external code.

### Terminal client

Erase a single disk using the default method: 

```bash
usody_sanitize -v /dev/sda  
```


## Issues

### hdparm Error: The running kernel lacks CONFIG_IDE_TASK_IOCTL support for this device

When trying to run the command 
`hdparm --user-master u --security-erase UsodyPassword /dev/sdX`, an error
message was encountered:

    The running kernel lacks CONFIG_IDE_TASK_IOCTL support for this device.

A return code of 22 from the `hdparm` command generally indicates 
that the command completed successfully, but some features may not have been 
fully supported on the device being used. This can happen if the device does 
not fully comply with the ATA standard, or if the device is a SCSI drive rather
than an ATA drive. It may also indicate that the specific options used in the
command are not supported on the device.

This error message is indicating that the kernel (the core part of the 
operating system) does not have support for the IDE task IOCTL feature, which
is required to run the `hdparm` command.

The `hdparm` command is used to configure and retrieve information about ATA 
hard drives, and the specific option used in the command (`--user-master u 
--security-erase UsodyPassword`) is used to erase the security settings on the
drive. Because the kernel does not have the necessary support, the command
cannot be executed.

To enable the CONFIG_IDE_TASK_IOCTL support in the kernel, it must be 
recompiled with this option enabled. The process for doing this will depend on
the specific distribution of Linux being used, but generally the steps are:

1. Download the source code for the current kernel version.
2. Extract the source code and navigate to the root directory.
3. Run `make menuconfig` or `make xconfig` to open the kernel configuration
menu.
4. Search for the option `CONFIG_IDE_TASK_IOCTL` and enable it.
5. Save the configuration and exit the menu.
6. Run make to compile the kernel with the new configuration.
7. Install the new kernel, and reboot the system to use it.

It's important to note that recompiling the kernel is a complex and delicate
process and it's recommended to have experience with Linux kernel compilation.

#### What is `CONFIG_IDE_TASK_IOCTL`

Is a kernel configuration option that enables support for the IDE task file
register IOCTLs in the Linux kernel. The IDE task file register is a set of
registers on an IDE (Integrated Drive Electronics) hard drive that are used to
control the drive's operations.

When this feature is enabled, the kernel provides an interface for user-space 
programs, such as `hdparm`, to access these registers and perform various 
operations on the hard drive, such as reading and setting parameters, 
performing security commands, and reading SMART data.

If this feature is not enabled, the `hdparm` command will not be able to access 
the task file register and will not be able to perform certain operations on 
the drive, such as security commands or SMART data.

It's worth noting that this feature is not only specific to `hdparm`, but also 
other utilities that can access the task file register, such as smartctl and 
hdparam will also be affected by the state of this feature.

It's also worth noting that this feature is specific to ATA drives and will not
have any effect on SCSI drives.
